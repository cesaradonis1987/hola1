<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>
        <x-jet-validation-errors class="mb-4" />



        <div class="container">
            <div class="row justify-content-center mt-2">
              <div class="col-lg-5 col-md-6 col-sm-12">
                <div class="card shadow">
                  <div class="card-title text-center border-bottom">
                    <h2 class="p-3">REGISTRO</h2>
                  </div>






        
            <div class="card-body">
        <form method="POST" action="{{ route('register') }}" class="">
            @csrf
            <div class="mb-3">
                <x-jet-label for="name" value="{{ __('Nombre') }}" class="form-label font-weight-bold"/>
                <x-jet-input id="name" class="form-control bg-dark-x border-0" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" placeholder="Ingresa tu nombre"/>
            </div>

            <div class="mb-3">
                <x-jet-label for="email" value="{{ __('Correo') }}" class="form-label font-weight-bold"/>
                <x-jet-input id="email" class="form-control bg-dark-x border-0" type="email" name="email" :value="old('email')" required placeholder="Ingresa tu correo"/>
            </div>

            <div class="mb-3">
                <x-jet-label for="password" value="{{ __('Contraseña') }}" class="form-label font-weight-bold"/>
                <x-jet-input id="password" class="form-control bg-dark-x border-0" type="password" name="password" required autocomplete="new-password" placeholder="Ingresa tu contraseña"/>
            </div>

            <div class="mb-3">
                <x-jet-label for="password_confirmation" value="{{ __('Confirmar Contraseña') }}" class="form-label font-weight-bold"/>
                <x-jet-input id="password_confirmation" class="form-control bg-dark-x border-0" type="password" name="password_confirmation" required autocomplete="new-password" placeholder="Confirma tu contraseña"/>
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
            <div class="mb-3">
                    <x-jet-label for="terms">
                        <div class="d-flex justify-content-center">
                            <x-jet-checkbox name="terms" id="terms" class="form-check-input"/>

                            <div class="ml-2" type="checkbox">
                                {!! __(' Acepto las condiciones de servicio y política de privacidad ', [
                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                ]) !!}
                            </div>
                        </div>
                    </x-jet-label>
                </div>
            @endif

            <div class="flex items-center justify-end mt-4">
                

                <x-jet-button class="btn btn-primary w-100">
                    {{ __('Registrar') }}
                </x-jet-button>

                <div class="text-center px-lg-5 pt-lg-3 pb-lg-4 p-4 w-100 mt-auto">
                <p class="d-inline-block mb-0">¿Tienes una cuenta?</p>
                <a class=" font-weight-bold text-decoration-none text-center" href="{{ route('login') }}">
                    {{ __('Ingresa desde aqui') }}
                </a>
                </div>
            </div>
        </form>
            </div>
        </div>
            </div>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
