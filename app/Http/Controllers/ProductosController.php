<?php

namespace App\Http\Controllers;


use App\Models\Productos;
use App\Models\Ventas;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos=Productos::all();
        $ventas=Ventas::all();
        return view('lista_productos', compact('productos','ventas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('agregar_producto');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $datos)
    {
        //
        $producto= new Productos();
        $producto->nombre=$datos->nombre;
        $producto->descripcion=$datos->descripcion;
        $producto->categoria=$datos->categoria;
        $producto->precio=$datos->precio;
        $producto->cantidad=$datos->cantidad;
        $producto->imagen=$datos->imagen->hashName();
        $datos->imagen->store('public');
        $producto->save();
        return redirect()->route('productos.index')->with('mensaje','Producto agregado correctamente');
        // GUARDAR LOS DATOS PROCESADOS DE UN FORMULARIO ---> POST
        //Productos::create($datos->all());
        //return redirect()->route('productos.index')->with('mensaje','Diseñador agregado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $producto)
    {
        //
        // MOSTRAR LOS DATOS/INFORMACIÓN DE UN ASPIRANTE EN ESPECIFICO
        
        return view('detalle_producto',compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Productos $producto)
    {
        //
        // DEVOLVER UN FORMULARIO CON LOS DATOS PRECARGADOS PARA EDITARLO --> GET 
        return view('editar_producto',compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $datos, Productos $producto)
    {
        //
        


        $producto->update([
            'nombre' =>$datos->nombre,
            'descripcion'=> $datos->descripcion,
            'categoria'=> $datos->categoria,
            'precio'=> $datos->precio,
            'cantidad'=> $datos->cantidad,
            'imagen'=> $datos->imagen->hashName(),
            ]);
            
        $datos->imagen->store('public');
        $producto->save();
        

        return redirect()->route('productos.index')->with('mensaje','Producto agregado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Productos $producto)
    {
        //
        // ELIMINA UN ASPIRANTE DE LA BD 
        $producto->delete();
        return back()->with('mensaje','El producto ha sido eliminado correctamente');
    }
}
