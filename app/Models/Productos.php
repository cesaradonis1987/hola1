<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Productos extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'productos';
    protected $fillable=['nombre', 'descripcion', 'categoria', 'precio', 'cantidad', 'imagen'];

}
