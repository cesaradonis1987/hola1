<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use App\Models\Ventas;

use Illuminate\Http\Request;

class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $productos=Productos::all();
        return view('catalogo', compact('productos'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $datos)
    {
        //'producto', 'cantidad', 'coste', 'idcliente', 'idproducto', 'imagen'
        $ventas = new Ventas();
        $ventas->productos=$datos->nom;
        $ventas->cantidad=$datos->cant;
        $ventas->coste=$datos->cost;
        $ventas->idcliente=$datos->idcli;
        $ventas->clientenomb=$datos->name;

        $ventas->idproducto=$datos->idpro;
        
        $ventas->save();
        return redirect()->route('catalogo.index')->with('mensaje','Compra realizada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
