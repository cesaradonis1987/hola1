<x-app-layout>

@section('Titulo')
PaperWeb
@endsection





@section('contenido')

<!-- inicia carusel -->

<div class="row g-0 mt-4">
  <div class="col-lg-12 d-block">
  <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">

    <div class="carousel-indicators">
      <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
      <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
      <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
      <div class="carousel-item active" data-bs-interval="10000">

        <div id="imagen">
        
        
        <img src="{{asset('adjuntos/banner.jpg')}}" class="d-block w-100 " alt="..." height="525px">

      </div>

        <div id="h33" class="carousel-caption d-block">
          <h3>Descuentos</h3>
          <p>Las Solo lo mejor PaperWeb.</p>
        </div>
      </div>
      <div class="carousel-item" data-bs-interval="2000">
        <img src="{{asset('adjuntos/banner2.jpg')}}" class="d-block w-100" alt="..."  height="525px">
        <div id="h33" class="carousel-caption d-block ">
          <h3>Increibles Descuentos</h3>
          <p>PaperWeb ¿Que haces perdiendo el tiempo?.</p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="{{asset('adjuntos/banner3.jpg')}}" class="d-block w-100" alt="..." height="525px">
        <div id="h33" class="carousel-caption d-block">
          <h3>¿Aun no compras nada?
          </h3>
          <p>Ya profa ponganos 10 porfas.</p>
        </div>
      </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  </div>
  </div>
</div>
  <!-- finalza carusel -->

@endsection

</x-app-layout>