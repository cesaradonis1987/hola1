<x-app-layout>
    @section('Titulo')
    PaperWeb
@endsection





@section('contenido')

<div class=" p-1 mt-3"> 
  <h2 class="text-center m-0">¿Quienes somos?</h2>
  </div>
  <div class="bg-warning p-1 mt-1"> 
    <h1 class="text-center m-0 text-light"></h1>
    </div>
    <div class="card  mt-2" >
      
        <div class="row g-0">
          <div class="col-md-2 d-flex justify-content-center">
            <a title="Los Tejos" href="https://www.google.com/maps/place/Cancún,+Q.R./@21.1620102,-86.8534812,17.25z/data=!4m5!3m4!1s0x8f4c2b05aef653db:0xce32b73c625fcd8a!8m2!3d21.161908!4d-86.8515279"><img src="{{asset('adjuntos/paper.jpg')}}"  alt="..." class="w-100" ></a>
          </div>

          <div class="col-md-10">
            <div class="card-body ">
              <p class="card-text text-justify">Ofresemos todo tipo de herramientas papeleras de las empresas y la papelería corporativa es uno de los aspectos más importantes de branding y de imagen corporativa que deben cuidar todas pues a través de ella se proyecta la imagen de nuestra empresa al exterior, en muchas ocasiones la tarjeta de visita es el primer elemento de comunicación de nuestra empresa que ve un futuro cliente.</p>
              
            </div>
          </div>
        </div>
      </div>

      <div class=""> 
        <h3 class="text-center m-0">Ubicacion</h3>
        </div>
        <div class="bg-warning p-1 mt-1"> 
          <h1 class="text-center m-0 text-light"></h1>
          </div>
      <div class="card mb-12 mt-2" >
      
        <div class="row g-0">
          

          <div class="col-md-9">
            <div class="card-body ">
              <p class="card-text text-justify"> Estamos ubicados en multiples calles de cancun nuestros locales varian desde locales en plazas hasta fraccionamientos, para mas informacion consulta nuestros telefono 998100000 o nuestros correos paperweb@hotmail.vom de igual manera puedes darle clic a la imagen para ver la ubicacion asi mismo le propoercionamos la direcion de nuestro establecimiento en la nuestra casa donde todo empezo 92, 77516 Cancún, Q.R..
              </p>
              
            </div>
          </div>

          <div class="col-md-3 d-flex justify-content-center">
            <a title="Los Tejos" href="https://www.google.com/maps/place/Cancún,+Q.R./@21.1620102,-86.8534812,17.25z/data=!4m5!3m4!1s0x8f4c2b05aef653db:0xce32b73c625fcd8a!8m2!3d21.161908!4d-86.8515279"><img src="{{asset('adjuntos/ubi.jpg')}}"  alt="..." class="w-100" ></a>
          </div>

        </div>
      </div>


@endsection
       
</x-app-layout>