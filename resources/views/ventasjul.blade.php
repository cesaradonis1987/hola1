<x-app-layout>

    @section('Titulo')
    PaperWeb
    @endsection
    
    
    
    
    
    @section('contenido')
    <div class=" p-3 mt-4"> 
        <h1 class="text-center m-0 ">COMPRAS JULIO</h1>
        </div>
        <a href="{{route('ventase.index')}}" class="btn btn-warning mt-3" >Enero</a>
    <a href="{{route('ventasfe.index')}}" class="btn btn-warning mt-3" >Febrero</a>
    <a href="{{route('ventasmar.index')}}" class="btn btn-warning mt-3" >Marzo</a>
    <a href="{{route('ventasa.index')}}" class="btn btn-warning mt-3" >Abril</a>
    <a href="{{route('ventasmay.index')}}" class="btn btn-warning mt-3" >Mayo</a>
    <a href="{{route('ventasjun.index')}}" class="btn btn-warning mt-3" >Junio</a>
    <a href="{{route('ventasjul.index')}}" class="btn btn-warning mt-3" >Julio</a>
    <a href="{{route('ventasago.index')}}" class="btn btn-warning mt-3" >Agosto</a>
    <a href="{{route('ventassep.index')}}" class="btn btn-warning mt-3" >Septiembre</a>
    <a href="{{route('ventasoct.index')}}" class="btn btn-warning mt-3" >Octubre</a>
    <a href="{{route('ventasnov.index')}}" class="btn btn-warning mt-3" >Noviembre</a>
    <a href="{{route('ventasdic.index')}}" class="btn btn-warning mt-3" >Diciembre</a>
    <a href="{{route('ventas.index')}}" class="btn btn-danger mt-3" >Todas</a>
    
    <table class="table  mt-4" id="data" class="display">
        <thead class="table-Light">
            <tr>
              <th scope="col">Articulo</th>
              <th scope="col">Cliente</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Costo</th>
              <th scope="col">Fecha</th>
              
              
    
            
              
            </tr>
          </thead>
    
    
          
        @foreach ($ventas as $ventas)
    
    
        
        <tbody>   
            
            <tr>
                <td>{{ $ventas->productos }}</td>
                <td>{{ $ventas->clientenomb }}</td>
                <td>{{ $ventas->cantidad }}</td>
                <td>{{ $ventas->coste }}</td>
                <td>{{ $ventas->created_at }}</td>
            </tr>
            
            
            
        @endforeach
    
        
    </table>
    
    @endsection
    
    </x-app-layout>