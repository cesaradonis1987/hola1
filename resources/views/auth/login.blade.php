<x-guest-layout>
    <x-jet-authentication-card>
        

<!-- Login Form -->
<div class="container">
    <div class="row justify-content-center mt-5">
      <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="card shadow">
          <div class="card-title text-center border-bottom">
            <h2 class="p-3">ACCESO</h2>
          </div>

          <x-jet-validation-errors class="mb-4" />
          @if (session('status'))
          <div class="mb-4 font-medium text-sm text-green-600">
              {{ session('status') }}
          </div>
      @endif

      

          <div class="card-body">
            <form method="POST" action="{{ route('login') }} " class="mb-4">
                @csrf

              <div class="mb-4">
                <x-jet-label for="email" value="{{ __('Correo') }}" class="mb-3" />
                <x-jet-input id="email" class="form-control bg-dark-x  border-0 mb-1" id="exampleInputEmail1" 
                type="email" name="email" :value="old('email')" placeholder="Ingresa tu email" required autofocus />
              </div>
              <div class="mb-4">
                <x-jet-label for="password" value="{{ __('Contraseña') }}" />
                <x-jet-input id="password" class="form-control bg-dark-x  border-0 mb-1" 
                type="password" name="password" placeholder="Ingresa tu contraseña" required autocomplete="current-password" />
              </div>
              <div class="mb-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" type="checkbox"  class="form-check-input"/>
                    <span class="ml-2 text-sm text-gray-600">{{ __('Recuerdame') }}</span>
                </label>
              </div>
              <div class="d-grid">
                
                <x-jet-button class="btn btn-primary">
                    {{ __('Iniciar Sesion') }}
                </x-jet-button>
              </div>
            </form>
            <div class="text-center px-lg-5 pt-lg-3 pb-lg-4  w-100 mt-auto">
                    <p class="d-inline-block mb-0">¿No tienes una cuenta?</p> <a href="{{ route('register') }}" class=" font-weight-bold text-decoration-none">Crear una ahora</a>
                </div>
          </div>
        </div>
      </div>
    </div>
  </div>





    </x-jet-authentication-card>
</x-guest-layout>
