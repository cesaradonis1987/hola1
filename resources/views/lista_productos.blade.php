<x-app-layout>



    @section('Titulo')
    PaperWeb
@endsection





@section('contenido')

<div class=" p-3 mt-2"> 
    <h3 class="text-center m-0 ">Lista de Productos</h3>
    </div>






    <a href="{{route('productos.create')}}" class="btn btn-warning mt-3" ><i class="fas fa-plus-circle"></i></a>
    

    
    @if(session()->has('mensaje'))
    <p style="color: rgb(12, 59, 146)">{{ session('mensaje') }}</p>
    @endif





    
    <table class="table  mt-2" id="data" class="display">
        <thead class="table-Light">
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Costo</th>
              <th scope="col">Cantidad</th>
              <th scope="col">Imagen</th>

              
              <th scope="col" class="text-center">Opciones</th>
              
            </tr>
          </thead>



        @foreach ($productos as $productos)
        <tbody>    
            <tr>
                


                <td>{{ $productos->nombre }}</td>
                
                <td>{{ $productos->precio }}</td>
                <td>{{ $productos->cantidad }}</td>
                <td><img src="{{asset('storage/'.$productos->imagen)}}" 
                     alt="..." width="50px" height="50px"></td>
                
                    

                <td>
                    <div class="d-flex justify-content-center">
                <a href="{{ route('productos.show', $productos->id)}}" class="btn  btn-primary active mx-1 d-inline ">ver</a>
                <a href="{{ route('productos.edit', $productos->id)}}" class="btn  btn-primary active mx-1 d-inline">editar</i></a>
                
                    <form action="{{ route('productos.destroy', $productos->id) }}" method="post" class="d-inline mx-1">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary active">borrar</button>
                    </form>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
 


    
    
    

@endsection

</x-app-layout>