<x-app-layout>



    @section('Titulo')
    PaperWeb
@endsection





@section('contenido')


<div class=" p-1 mt-3"> 
  <h2 class="text-center m-0">Especificaciones</h2>
  </div>
  <div class="bg-warning p-1 mt-1"> 
    <h1 class="text-center m-0 text-light"></h1>
    </div>


<!-- Header -->
<div class="card mb-3">
  <div class="row justify-content-center">
  <div class="col-6">
    <div class="row justify-content-center">
  <div class="col-6">
  <img src="{{asset('storage/'.$producto->imagen)}}" class="card-img" alt="..." height="200px" width="200px">
  </div>
    </div>
  
  <div class="card-body">
    <h5 class="card-title">Articulo: {{ $producto->nombre}}</h5>
    <p class="card-text">Descripcion: {{ $producto->descripcion}}</p>
    <p class="card-text">Precio: {{ $producto->precio}}</p>
    <p class="card-text">Cantidad: {{ $producto->cantidad}}</p>
    <p class="card-text">Tipo: {{ $producto->categoria}}</p>
    <p class="card-text"><small class="text-muted">Ultima actualizacion {{$producto->updated_at}}</small></p>
  </div>
</div>
</div>

@endsection

</x-app-layout>