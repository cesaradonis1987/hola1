<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\VentasController;
use App\Http\Controllers\VentasaController;
use App\Http\Controllers\VentaseController;
use App\Http\Controllers\VentasfeController;
use App\Http\Controllers\VentasmarController;
use App\Http\Controllers\VentasmayController;
use App\Http\Controllers\VentasjunController;
use App\Http\Controllers\VentasjulController;

use App\Http\Controllers\VentassepController;
use App\Http\Controllers\VentasoctController;
use App\Http\Controllers\VentasnovController;
use App\Http\Controllers\VentasdicController;
use App\Http\Controllers\VentasagoController;



use App\Http\Controllers\CatalogoController;
use App\Models\Ventas;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('inicio');
    
})->name('inicio');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    $ventas=Ventas::all();
    return view('dashboard',compact('ventas'));
})->name('perfil');

Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventas', VentasController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasa', VentasaController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventase', VentaseController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasfe', VentasfeController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasmar', VentasmarController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasmay', VentasmayController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasjun', VentasjunController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasjul', VentasjulController::class);

Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasago', VentasagoController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventassep', VentassepController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasoct', VentasoctController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasnov', VentasnovController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('ventasdic', VentasdicController::class);




Route::view('acerca de', 'about')->name('about');
Route::resource('catalogo', CatalogoController::class);
Route::middleware(['auth:sanctum', 'verified' , 'admin'])->resource('productos', ProductosController::class);