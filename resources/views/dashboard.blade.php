<x-app-layout>

    


    @section('Titulo')
PaperWeb
@endsection





@section('contenido')


<div class=" p-3 mt-4"> 
    <h1 class="text-center m-0 ">PERFIL</h1>
    </div>
    <div class="bg-warning p-1 mt-1"> 
        <h1 class="text-center m-0 text-light"></h1>
        </div>

<div class="container-xl mt-3" >
    <div class="card text-center">
        


        <div class="card-body">
            <div class="row g-2">
                
                <div class="col-sm-4 " >
                    <div class="d-flex justify-content-center">
                    <img src="{{asset('adjuntos/user.png')}}" class=""  width="150px" height="150px">
                    </div>
                    
                </div>

                <div class="col-sm-8" >
                    <ul class="list-group list-group-horizontal-xxl" >
                        <li class="list-group-item text-justify">ID: {{Auth::user()->id}}</li>
                        <li class="list-group-item text-justify">Nombre: {{Auth::user()->name}}</li>
                        <li class="list-group-item text-justify">Correo: {{Auth::user()->email}}</li>

                      </ul>
                </div>
              </div>
        </div>
      </div>
</div>

</div>



<div class=" p-3 mt-4"> 
    <h1 class="text-center m-0 ">COMPRAS</h1>
    </div>
<table class="table  mt-2" id="data" class="display">
    <thead class="table-Light">
        <tr>
          <th scope="col">Articulo</th>
          <th scope="col">Cantidad</th>
          <th scope="col">Costo</th>
          
          

        
          
        </tr>
      </thead>


      @if (Route::has('login'))
      @auth
    @foreach ($ventas as $ventas)


    
    <tbody>   
        @if (Auth::user()->name===$ventas->clientenomb)
        <tr>
            <td>{{ $ventas->productos }}</td>
            
            <td>{{ $ventas->cantidad }}</td>
            <td>{{ $ventas->coste }}</td>
          
        </tr>
        @else
        
        @endif
        
    @endforeach

    @endauth
        @endif
</table>

@endsection








            
</x-app-layout>
