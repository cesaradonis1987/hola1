<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('Titulo')</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.2/css/all.css" integrity="sha384-vSIIfh2YWi9wW0r9iZe7RJPrKwp6bG+s9QZMoITbCckVJqGCCRhc+ccxNcdpHuYu" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="/css/aas.css">
        <link rel="stylesheet" href="/css/estilo123.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <script src="{{ mix('js/app.js') }}" defer></script>

        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap" rel="stylesheet">
    </head>
    
    <body style="font-family: 'Roboto Mono', monospace;">









      
      
        <!-- Menuuuuuuuuuuu-->
<nav class="navbar navbar-expand-lg navbar-light navbar-custom">
  
    <div class="container">
      

        <a class="navbar-brand" href="{{ route('inicio') }}">
          <img src="/adjuntos/paper.jpg" alt="" width="40" height="35" style="border-radius: 50%;" class="d-inline-block align-top">
          Paperweb</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        

        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">

          <ul class="navbar-nav ms-auto mb-2 mb-lg-0">

            


              
              <!-- Dropdown usuariooooooooooooooooooooooooooooooooo -->
            @if (Route::has('login'))
            <div >
                @auth

                            <div class="btn-group" role="group">
                              <button id="btnGroupDrop1" type="button" class="btn btn-default d-flex"  data-bs-toggle="dropdown" aria-expanded="false">
                                <img class="h-8 w-8 rounded-full object-cover mx-2"data-toggle="dropdown" src="{{asset('adjuntos/user.png')}}" alt="{{ Auth::user()->name }}" />
                                {{ Auth::user()->name }}
                              </button>
                              <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1">


                                <li><a href="{{ url('/dashboard') }}" class="dropdown-item">Perfil</a></li>

                                <li><a href="{{ route('profile.show') }}" class="dropdown-item">Editar</a></li>

                                <li>
                                    <form method="POST" action="{{ route('logout') }}">
                                        @csrf

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                     onclick="event.preventDefault();
                                            this.closest('form').submit();">Salir</a></li>


                              </ul>
                            </form>
                            </div>
                          </div>
                    
                    
                @else
                    <a href="{{ route('login') }}" class="btn btn-success">Ingresar</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="btn btn-danger">Registrar</a>
                    @endif
                @endauth
            </div>
        @endif



        <!-- Fin dropdown -->
            </li>
          </ul>
         
  

        </div>
      
      </div>
    </div>
    </nav>
    <nav class="nav justify-content-center navbar-custom1">
      <a class="nav-link btn " aria-current="page" href="{{ route('inicio') }}">Inicio</a>
      <a class="nav-link btn >" href="{{ route('about') }}">Acerca de</a>
      <a class="nav-link btn" href="{{ route('catalogo.index') }}">Articulos</a>
      @if (Route::has('login'))
            @auth
            @if (Auth::user()->rol==='ADM')
            
            
              <a class="nav-link btn" href="{{ route('productos.index') }}">Administrar</a>
              <a class="nav-link btn" href="{{ route('ventas.index') }}">Ventas</a>
            
            @endif
            @endauth
            @endif
      
    </nav>
        


            <!-- Page Content -->


            <div class="container">
              <div>
              @yield('contenido')
              </div>
                </div>

            <main>
                {{ $slot }}
            </main>
        </div>
        @stack('modals')
        @livewireScripts










        


<!-- Page foter -->
<div class="footer-basic">
  <footer>
      <div class="social"><a href="instagram.com/alexisrhz/"><i class="icon ion-social-instagram"></i></a><a href="https://www.snapchat.com/add/alexisrhz18"><i class="icon ion-social-snapchat"></i></a><a href="https://twitter.com/AlexisR98298681"><i class="icon ion-social-twitter"></i></a><a href="https://www.facebook.com/paul.riverahernandez"><i class="icon ion-social-facebook"></i></a></div>
      <ul class="list-inline">
          Siempre a su servicio proporcionandole los mejores articulos de papeleria que neceseita.
      </ul>
      <p class="copyright">Paperweb © 2021</p>
  </footer>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>


<!-- Page foter -->

 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>


    </body>
</html>
