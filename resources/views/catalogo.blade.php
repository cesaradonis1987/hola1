<x-app-layout>
    @section('Titulo')
PaperWeb
@endsection





@section('contenido')
<div class=" p-3 mt-4"> 
    <h1 class="text-center m-0">ARTICULOS</h1>
    </div>

    
    <div class="bg-yellow-500 p-1 mt-1"> 
        <h1 class="text-center m-0 text-light"></h1>
        </div>
   



<!-- START SECTION STORE -->
<section class="store mt-5">
  <div class="container">
      <div class="items">
          <div class="row g-2 d-flex justify-content-center">

            









            
              @foreach ($productos as $productos)
              
              <div class="card mb-3 ms-2  bg-dark-y" style="max-width: 500px;">
                <div class="row g-0  item shadow">
                  <div class="col-md-5">
                    <div class="row justify-content-center.">
                    <img src="{{asset('storage/'.$productos->imagen)}}" class="item-image  " alt="..." width="180px" height="180px" >
                </div>
                  </div>
                  <div class="col-md-7">
                    <div class="card-body ">
                        <h5 style="display: none;"class="item-id text-center">{{$productos->id}} </h5>
                        
                        
                        <h5 class="item-title text-center">{{$productos->nombre}} </h5>
                        
                            
                      <h5 class="item-price text-justify">${{$productos->precio}} </h5>
                      
                      <p class="card-text text-justify">{{$productos->descripcion}}</p>


                      @if (Route::has('login'))
                      @auth
                      <button class="item-button btn btn-warning addToCart w-100">AÑADIR </button>
                        @else
                          <a href="{{ route('login') }}" class="item-button btn btn-primary addToCart w-100">AÑADIR</a>
                      @endauth
                      @endif
                
                    </div>
                  </div>
                </div>
              </div>
                
                  


              @endforeach
                  

            


          </div>
      </div>
  </div>
</section>

<!-- END SECTION STORE -->
<!-- START SECTION SHOPPING CART -->
<section class="shopping-cart">
  <div class="container  container2">
      
      <h1 class="text-center"><i class="fas fa-shopping-cart"></i></h1>
      <div class="bg-yellow-500 p-1 mt-1"> 
        <h1 class="text-center m-0 text-light"></h1>
        </div>
      <hr>


      <div class="row">
          <div class="col-6 ">
              <div class="shopping-cart-header w-100">
                  <h6 class="text-center"><i class="fas fa-box-open"></i></h6>
              </div>
          </div>

          <div class="col-3">
              <div class="shopping-cart-header w-100">
                  <h6 class="text-truncate"><i class="fas fa-dollar-sign"></i></h6>
              </div>
          </div>
          <div class="col-3">
              <div class="shopping-cart-header">
                <h6 class=""><i class="fas fa-boxes"></i></h6>
              </div>
          </div>
      </div>
      <!-- ? START SHOPPING CART ITEMS -->
      <div class="shopping-cart-items shoppingCartItemsContainer">
      </div>
      <!-- ? END SHOPPING CART ITEMS -->

      <!-- START TOTAL -->
      <div class="row">
          <div class="col-12">
              <div class="shopping-cart-total d-flex align-items-center">
                  <p class="mb-0">Total</p>
                  <p class="ml-4 mb-0 shoppingCartTotal">$0</p>
                  <div class="toast ml-auto bg-info" role="alert" aria-live="assertive" aria-atomic="true"
                      data-delay="2000">
                      <div class="toast-header">
                          <span>✅</span>
                          <strong class="mr-auto ml-1 text-secondary">Elemento en el carrito</strong>
                          <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="toast-body text-white">
                          Se aumentó correctamente la cantidad
                      </div>
                  </div>

                  
                  <form  action="{{ route('catalogo.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id='foo0' name="nom" value=""/>
                    <input type="hidden" id='foo1' name="cant" value=""/>
                    <input type="hidden"  id='foo' name="cost" value=""/>
                    @if (Route::has('login'))
                    @auth
                    
                    <input type="hidden" name="idcli" value={{Auth::user()->id}}>
                    <input type="hidden" name="name" value={{Auth::user()->name}}>
                    @endauth
                    @endif
                    <input type="hidden" id='foo3'name="idpro" value=''>

                    @if (Route::has('login'))
                    @auth
                    <button class="btn btn-outline-danger ml-auto comprarButton" type="submit" data-toggle="modal"

                      data-target="#comprarModal">Comprar</button>
                      @else
                        <a href="{{ route('login') }}" class="btn btn-success ml-auto">Comprar</a>
                    @endauth
                    @endif
                    </form>


              </div>
          </div>
      </div>

      <!-- END TOTAL -->

      <!-- START MODAL COMPRA -->
      
      <!--Fin compra -->


  </div>

</section>
<!-- Fin del carro -->







<!-- SCRIPTS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
  integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
  integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
  crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
  integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
  crossorigin="anonymous"></script>

<script src="{{('js/tiendita.js') }}"></script>






@endsection
       
</x-app-layout>